#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "IDataAccess.h"
#include"sqlite3.h"
#include "MyException.h"
#include <io.h>
#include <stdio.h>
#include <vector>
#include <algorithm>

#define FILE_NAME "galleryDB.sqlite"


class DatabaseAccess : public IDataAccess
{
public:
	DatabaseAccess();
	~DatabaseAccess() = default;

	// album related
	virtual const std::list<Album> getAlbums() override;
	virtual const std::list<Album> getAlbumsOfUser(const User& user) override;
	virtual void createAlbum(const Album& album) override;
	virtual void deleteAlbum(const std::string& albumName, int userId) override;
	virtual bool doesAlbumExists(const std::string& albumName, int userId) override;
	virtual Album openAlbum(const std::string& albumName) override;
	virtual void closeAlbum(Album& pAlbum) override;
	virtual void printAlbums() override;

	// picture related
	virtual void addPictureToAlbumByName(const std::string& albumName, const Picture& picture) override;
	virtual void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName) override;
	virtual void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;
	virtual void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId) override;

	// user related
	virtual void printUsers() override;
	virtual User getUser(int userId) override;
	virtual void createUser(User& user) override;
	virtual void deleteUser(const User& user) override;
	virtual bool doesUserExists(int userId) override;


	// user statistics
	virtual int countAlbumsOwnedOfUser(const User& user) override;
	virtual int countAlbumsTaggedOfUser(const User& user) override;
	virtual int countTagsOfUser(const User& user) override;
	virtual float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	virtual User getTopTaggedUser() override;
	virtual Picture getTopTaggedPicture() override;
	virtual std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	virtual bool open() override;
	virtual void close() override;
	virtual void clear() override;

	int findNextUserId() override;
	int findNextPictureId() override;


private:
	sqlite3* _db;
	static std::vector<std::string> table;
	Album openedAlbum;

	//Helper Functions
	bool SendQuery(const char* query);
	static int callback(void* data, int argc, char** argv, char** azColName);



};

